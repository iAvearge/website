This is a landing page for my domain "jsparrow.me".
If you wish to contact me either email admin@jsparrow.me or jsparrow200225@gmail.com
If you wish to contact me securely and privately please message on on Keybase by clicking this link: https://keybase.io/jsparrow200225/chat
This link will work whether you have an account or not.